# Building packages for Automotive

If you are interested in making automotive-related packages available for
AutoSD or the Red Hat In-Vehicle Operating System, there are three paths you
can pick from:

- Package the application in Fedora and bring it to EPEL
- Package the application in the Automotive SIG RPM repository
- See with your partner manager about having the application be made available
  by Red Hat

To understand which path is the best for you, we need to go back to the way code
flows into RHEL and Red Hat's automotive offering:

![Code Flow into AutoSD, CentOS Stream, RHEL and the Automotive product](../img/CodeFlow.jpg)


## Building packages for Fedora and EPEL

The [Fedora project](https://docs.fedoraproject.org/en-US/project/) is a community
project working together on multiple projects, the most well known one being
the Fedora Linux operating system.

Red Hat Entreprise Linux (RHEL) is based upon the work done in the Fedora Linux
distribution. So working on Fedora Linux gives you a preview of what will be
coming in RHEL, which Red Hat's automotive offering is built upon.

In addition to Fedora Linux, the Fedora community is working on a project called
[Extra Packages for Entreprise Linux](https://docs.fedoraproject.org/en-US/epel/),
aka EPEL. This project allows Fedora packagers to build and maintain packages
built against RHEL.
Packages that are part of EPEL are thus compatible with RHEL, CentOS Stream and
Red Hat's automotive offering.

One important thing to realize though is that EPEL packages are not allowed to
override RHEL packages, so if you want a package that already exists in RHEL but
with a different configuration or version, you will have to find a way to make
your package not conflict with the RHEL one.

You can find how to become a Fedora packager in Fedora's documentation on [how
to join the package maintainers](https://docs.fedoraproject.org/en-US/package-maintainers/Joining_the_Package_Maintainers/).


## Building packages for the Automotive SIG RPM repository

While Fedora Linux gives you a look far ahead into what will be in RHEL,
building in the Automotive SIG RPM repository allows you to build against AutoSD
which is itself the future of Red Hat's automotive offering based on RHEL.
Therefore building against AutoSD gives you a look into a closer future.

Building packages for this repository is entirely controlled by the Automotive SIG,
so the procedure is simpler.

### What can be packaged?

The first and most important criteria is that the code must be under a Free and
Open Source license.

Unlike EPEL, the Automotive SIG repository can override packages that are in
RHEL or even AutoSD. This allows the SIG to experiment with new versions or
different configurations than what Red Hat has in its product.

### How to request a package?

Since the SIG controls the process, if you would like to package something in the
Automotive SIG RPM repository, you can simply send an email to the [
centos-automotive-sig](https://lists.centos.org/mailman/listinfo/centos-automotive-sig)
mailing list to present the project you would like to package.

### How to package?

A git repository will be created for you under: [https://gitlab.com/CentOS/automotive/rpms/](https://gitlab.com/CentOS/automotive/rpms/)

You will be able to place there your spec file and patches.

The tarball of the sources of the project should be uploaded to the lookaside cache
(which is a place where we store the sources of everything we build, should the
upstream project disapear tomorrow, we will still be able to reproduce the build
we made 6 months ago).
You can upload to the lookaside cache using the `lookaside_upload_sig` script that
can be found in the [centos-git-common](https://git.centos.org/centos-git-common)
project (ensure you have a recent version).
```
lookaside_upload_sig -f <tarball> -n <pkg_name>
```

Once your sources are uploaded to the lookaside cache, you will have to create
a `sources` file in your git repository (next to the spec file). This allows
the build system to know which archive to retrieve from the lookaside cache to
do the build. The file can simply be created using the command:
```
sha512sum --tag <tarball> > sources
```

When you have pushed to GitLab your spec file, your `sources` file and, potentially,
your patches, you will be able to build your package in [CBS](https://cbs.centos.org/),
the CentOS Build System. You will use the `cbs` command provided by the `centos-packager`
package in both Fedora and CentOS to interact with CBS.

The first time, you will need to add your package to the [tag](#automotive-sig-cbs-tags) you want to build
against, you can do this via:
```
cbs add-pkg --owner=<username> <tag> <pkg_name>
```
You will need to do this to the `-candidate`, `-testing` and `-release` tags.


After that, you can start the build:
```
cbs build <tag> git+https://gitlab.com/centos/automotive/rpms/<pkg_name>.git#<git_hash>
```
If you want to build the latest commit of the branch you're in, you can use:
```
cbs build <tag> git+https://gitlab.com/centos/automotive/rpms/<pkg_name>.git#`git log -1 --format=format:"%H"`
```

Once the build is successful, you can push it to the "testing" repository:
```
cbs move <candidate_tag> <testing_tag> <pkg_name-version-release>
```
This will make the package show up in: [https://buildlogs.centos.org/9-stream/automotive/](https://buildlogs.centos.org/9-stream/automotive/)


Finally, you can push the package to the CentOS mirror network by pushing it
to the "stable" repository:
```
cbs move <testing_tag> <release_tag> <pkg_name-version-release>
```
This will make the package show up in: [http://mirror.stream.centos.org/SIGs/9-stream/automotive/](http://mirror.stream.centos.org/SIGs/9-stream/automotive/)

### Automotive SIG CBS tags

CBS tags are used to "classify" packages in the build system based on their
destination.

The Automotive SIG RPM repository uses the following tags:

* **automotive9s-packages-main-el9s**: This is the tag you will specify in the `cbs build` above.
  It corresponds to the main Automotive SIG RPM repository. It is currently our only repository
  but we may have more in the future.
* **automotive9s-packages-main-el9s-build**: This is the tag in which your build will
  happen.
* **automotive9s-packages-main-candidate**: This is the tag in which your build will
  land once it's built.
* **automotive9s-packages-main-testing**: This is the tag used to mark packages that
  should land in the testing repository.
* **automotive9s-packages-main-release**: This is the tag used to mark packages that
  should land in the stable repository.


## Building packages for AutoSD

AutoSD is the public, in development, version of the Red Hat Automotive product.
This means that changes landing in AutoSD are meant to land in the product in
the future. Rollback could happen, but they should be really exceptional.

Since AutoSD is the public, in development, version of a Red Hat product, its
direct access is restricted to Red Hat employees, however, anyone in the community
can propose changes to AutoSD. These proposals can take multiple forms, from a
pull-request (or merge request) opened against an AutoSD package, to a post on the
[centos-automotive-sig](https://lists.centos.org/mailman/listinfo/centos-automotive-sig)
mailinglist, or reaching out to your Red Hat partner manager, if you have one
(which may be a good idea to do if you have such relation, even if you open a
pull/merge-request).

### Where are the AutoSD sources?

AutoSD is based on CentOS Stream with some divergences where needed for the
automotive industry.

So, if you are looking for the AutoSD sources, you will find most of them in
CentOS Stream at: [https://gitlab.com/redhat/centos-stream/](https://gitlab.com/redhat/centos-stream/).

The [`rpms`](https://gitlab.com/redhat/centos-stream/rpms) namespace contains
the git repositories constituting dist-git (ie: the git repositories with the
spec files and patches for building the CentOS Stream RPMs).
The [`src`](https://gitlab.com/redhat/centos-stream/src) namespace contains
[Source-git](https://docs.centos.org/en-US/stream-contrib/contributing/source-git/)
repository for CentOS Stream 8 and 9.

For the packages that diverge from CentOS Stream for AutoSD, you can find their
sources at: [https://gitlab.com/CentOS/automotive](https://gitlab.com/CentOS/automotive).
That namespace use the same structure with the [`rpms`](https://gitlab.com/CentOS/automotive/rpms)
and [`src`](https://gitlab.com/CentOS/automotive/src) namespaces.

!!! note

    The sources for the Automotive SIG packages may also be kept in
    [https://gitlab.com/CentOS/automotive](https://gitlab.com/CentOS/automotive), so
    it is recommended that maintainers use different branches for AutoSD and
    the Automotive SIG with clear naming.

### Building for AutoSD

AutoSD packages are built on the CentOS Build System ([CBS](https://cbs.centos.org/)).
The procedure to build those packages is thus similar to the procedure to build
RPMs for the Automotive SIG RPM repository, see the section above. The only
difference is in the CBS tags used (see next section).

### AutoSD CBS tags

CBS tags are used to "classify" packages in the build system based on their
destination.

The Automotive SIG RPM repository uses the following tags:

* **autosd9s-packages-main-el9s**: This is the tag you will specify in the `cbs build` above.
  It corresponds to the main AutoSD RPM repository. It is currently our only repository
  but we may have more in the future.
* **autosd9s-packages-main-el9s-build**: This is the tag in which your build will
  happen.
* **autosd9s-packages-main-candidate**: This is the tag in which your build will
  land once it's built.
* **autosd9s-packages-main-testing**: This is the tag used to mark packages that
  should land in the testing repository.
* **autosd9s-packages-main-release**: This is the tag used to mark packages that
  should land in the stable repository.
