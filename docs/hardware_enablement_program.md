# Hardware Enablement Program

The hardware enablement program is thoughtfully crafted to foster collaboration
and enable the development of intelligent and connected vehicle applications.
Its primary objective is to streamline and facilitate the process of enabling
and certifying[^1] reference hardware platforms for the Red Hat In-Vehicle
Operating System (RHIVOS). By embracing an open source approach, we believe
hardware suppliers can reap numerous benefits while contributing to a vibrant
ecosystem.

Our program empowers Silicon partners to bring their hardware platforms into 
[CentOS Automotive Stream Distribution (AutoSD)](https://blog.centos.org/2022/03/centos-automotive-sig-announces-new-autosd-distro/)
and the Red Hat In-Vehicle OS. AutoSD functions as an upstream repository to the
Red Hat automotive product, comparable to how CentOS Stream operates in relation
to Red Hat Enterprise Linux (RHEL). AutoSD, although based on CentOS Stream with
a few divergences, serves as our community presence. RHIVOS is Red Hat's
software-defined vehicle functionally safe certified OS solution (currently in
development).

To cater to different silicon partner needs, we have established two distinct
tracks: 

* the experimental/development track
* the production track. 

The experimental/development track is ideal for partners who prefer to embark
on the process independently, providing them with the opportunity to test the
compatibility of their hardware with AutoSD. This initial testing phase allows
silicon partners to gain valuable insights and make adjustments as needed.

The production track offers a comprehensive solution for silicon partners ready
to release their hardware platforms to the market, supported by Red Hat 
In-Vehicle OS. This track encompasses enablement, integration, and a rigorous 
hardware certification process that ensures the reference platform meets Red 
Hat's stringent stability, reliability, and security requirements that the 
automotive industry expects from us. Once certified, the hardware platform will
proudly join the official list of supported devices, thereby becoming accessible
to RHIVOS customers.

Our hardware enablement program aims to provide silicon partners with an
efficient and straightforward process to seamlessly integrate their hardware
platforms into the RHIVOS ecosystem. By adopting an open source model, we can
leverage the collective knowledge and expertise of the community, fueling
innovation and continuous improvement. Engaging with open source technologies
not only encourages collaboration and knowledge sharing but also enhances the
quality and reliability of the solutions we create together.

It is important to note that hardware suppliers have the flexibility to work on
one or multiple tracks simultaneously if they wish. For instance, a partner
might opt to begin with the experimental/development track to swiftly validate
a proof of concept while concurrently dedicating efforts to upstream their
drivers and initiate progress on the production track. This parallel approach
allows silicon partners to explore different avenues and tailor their journey
to best suit their unique requirements.

Upstreaming of hardware drivers to the Linux kernel mainline provides hardware
suppliers with tangible advantages:

1. Improved Compatibility: Inclusion in the mainline kernel ensures broader 
   Linux distribution support and provides hardware compatibility out-of-the-box.
   Out-of-tree drivers will continuously need to maintain compatibility with the
   baseline.
2. Reduced Maintenance Burden: Linux community support means shared 
   responsibility for driver maintenance, freeing up silicon partner resources
   for other critical areas. Includes the capability to maintain fewer versions
   of OS in-house and it allows for broader testing.
3. Increased Visibility and Market Adoption: Mainline inclusion boosts mindshare
   and compatibility of hardware, leading to greater market adoption and brand
   recognition
4. Collaboration and Feedback: Community collaboration provides valuable
   feedback, leading to higher quality drivers improved performance, and
   enhanced reliability

## The Ideal Hardware

The following represents a set of requirements and desires for the reference
hardware and enabling software. In the spirit of collaboration, these points
will be considered as aspirations during the experimental/development track.
However, as we progress towards the production track, these items will be
treated more definitively as requirements (exceptions will be discussed on a
case-by-case basis).

1. It is highly desirable that hardware drivers are well-supported and meet the
   standards set by Red Hat[^2] in the upstream relevant to the RHEL/RHIVOS
   stack. By aligning with Red Hat's acceptable driver criteria, hardware
   suppliers can ensure seamless integration and compatibility with our software
   ecosystem.
2. Hardware suppliers are encouraged to collaborate and work with current Red
   Hat build environment technology, leveraging tools such as RPM (
   [Red Hat Package Manager](https://rpm.org/)) and
   [OSTree](https://ostreedev.github.io/ostree/). This compatibility facilitates
   a smooth and efficient integration process, reducing complexities and
   enhancing interoperability.
3. To enable advanced virtualization capabilities, the hardware platform should
   provide EL2 privileges for aarch64 architecture or "ring-1" for the x86
   architecture. This allows the implementation of KVM (Kernel-based Virtual
   Machine) and enables the launching of virtual machines, thus expanding the
   possibilities for intelligent and connected vehicle applications.
4. It is advantageous for the hardware to include a capable GPU that has been
   upstreamed. This ensures optimal performance and support for
   graphics-intensive applications, contributing to a richer user experience
   and opening doors to a broader range of automotive use cases.
5. The hardware (specifically aarch64 architecture) should support one of the
   standard ARM [SystemReady](https://www.arm.com/architecture/system-architectures/systemready-certification-program)
   profiles. For example, compliance with profiles like SystemReady-IR or
   SystemReady-ES would enhance the system's reliability, robustness, and
   compatibility with industry standards

## Workflow for Hardware Enablement

The diagram below describes the different tracks for hardware enablement. The
experimental/development track allows the hardware suppliers to work mostly
independently to enable hardware to work with CentOS AutoSD.

The hardware supplier will have several options to get to their preferred
solution within the experimental track. The supplier may choose to develop their
drivers in either Linux-next, Fedora, or CentOS AutoSD. Red Hat recommends that
even within the experimental track, the hardware drivers are upstreamed to
Linux-next to speed up production enablement.

Once the hardware supplier and Red Hat agree to move forward with the production
track, it is expected that the drivers are [upstreamed](
https://www.redhat.com/en/blog/what-open-source-upstream) (see
[High-Level Guidance for Upstreaming of Drivers](#high-level-guidance-for-upstreaming-of-drivers))
into the Linux-next kernel, and Red Hat will backport them in the next possible
RHIVOS kernel release.

Red Hat will provide the silicon partner with a Hardware Test Suite to be run
during the Production Track. The Test Suite consists of procedures necessary to
certify hardware on Red Hat software. A Test Suite guide gives an overview of
the entire certification process, explains how to set up the certification
environment, test the systems or components being certified, and submit the
results to Red Hat for verification. The guide also provides background
information including the test methodology and results evaluation.

Once these test results are reviewed and approved by Red Hat, the hardware
platform will be published in the Red Hat catalog.

![Workflow Diagram](img/HWEnablementFlow.png)

## High-Level Guidance for Upstreaming of Drivers

As a hardware supplier, upstreaming hardware drivers is crucial for seamless
integration into the open source ecosystem. Here's a quick guide outlining the
steps hardware suppliers can take to upstream their hardware drivers
effectively. For Fedora development, see [Fedora/ARK Kernel documentation](
https://cki-project.gitlab.io/kernel-ark/).

1. Start by checking out Linus's master branch, using the following command:

    ```git
    git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    ```

2. Begin with the upstream arm64 defconfig kernel configuration. If necessary,
   you can add options for your specific board that may be missing.
   **Note:** If you compile your drivers with the kernel source, it may mask any
   changes made to the core parts of the kernel that Red Hat ships.

3. If you need to make code changes to the kernel sources in step 2, you have
   some options available:
    1. modify the architecture of the module to avoid the need to change the
       kernel itself
    2. propose changes to the upstream kernel to accommodate the specific needs
       of your module
    3. upstream the driver. This involves contributing the driver code to the
       mainline Linux kernel sources, making it available to the broader open
       source community

4. For kernel modules that aren't yet upstream and that you plan to ship
   independently, you can compile them as out-of-tree modules. The process for
   this can be found in detail at [https://docs.kernel.org/kbuild/modules.html](
   https://docs.kernel.org/kbuild/modules.html)

5. You are in good shape if you don't need to make any code changes to the
   kernel source in step 2.

For a complete guide on how to upstream drivers into the Linus Kernel, see [
A guide to the Kernel Development Process](
https://docs.kernel.org/process/development-process.html).

## Getting Started

If you are interested in working within the experimental/development track, send
a request to the [Automotive SIG mailing list](https://lists.centos.org/mailman/listinfo/centos-automotive-sig).
If you are interested in inquiring about the production track, please email
[automotive-requests@redhat.com](mailto:automotive-requests@redhat.com).


<!-- Footnotes themselves at the bottom. -->

[^1]:
     Note that Functional Safety certification is done separately, but relies
     on hardware certification

[^2]:
     Some out-of-tree drivers can be discussed on a case-by-case basis

